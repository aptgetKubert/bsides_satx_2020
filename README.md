# TTP Aggregators for MITRE ATT&CK

## Purpose
Both of these scripts were created in order to make it easier for hunt team leaders/planners to quickly identify shared TTPs between adversary groups and malware/software sets those group use. By knowing this information, defenders can orient themselves to the most likely TTPs that could be used in their environment and what they should start looking for first. 

## Supporting Files
Both the Group and Software TTP Aggregators need certain CSV files in order to properly compile and calculate the shared TTPs. These CSV files are created by a corresponding .py file which pulls data directly from MITRE. The aggregation scripts were built in this way to allow for greater flexibility on the defender's part. For example, if you had proprietary information about a particular group's techniques, then you could manually add that information to the CSV files, or even create your own as long as they follow the given format. The idea here is to allow for each organization to compliment the public information provided by MITRE with their own threat intelligence sources.

| CSV File | Creation Script | Purpose |
| ------ | ------ | ----- |
| MITRE_group_output.csv | MITRE_TTP_to_Groups.py | Maps MITRE identified groups to TTPs |
| MITRE_software_output.csv | MITRE_TTP_to_Software.py | Maps MITRE identified software/malware to TTPs |
| MITRE_data_sources_output.csv | MITRE_TTP_to_DataSources.py | Maps TTPs to potential data sources defenders can use to find them  |
| MITRE_software_to_groups_output.csv | MITRE_Groups_to_Software.py | Maps groups to the software/malware they are known to use |

Both aggregation scripts assume the supporting CSV files will be in the running directory, so it is recommended all scripts are run from the same folder, otherwise you will need to edit the code to point to the CSV location.

## Python Version

All scripts were written in Python 3.8.3, 64-bit on Microsoft Visual Code Studio. In order for these to work you will need Python 3.

## Usage

Both Group TTP Aggregator and Software TTP Aggregator are commandline tools. They can be passed specific commandline arguments, or the user can manually provide the required information. There are options in both scripts to allow the user to add multiple groups/malware at the same time based on suspected country of origin and targeted industry. In the case of Software TTP Aggregator, you can also add all the software/malware used by a particular adversary group. 

| Flag | Full Flag | Purpose |
| ------ | ------ | ----- |
| -h | --help | Prints the help message for the script |
| -c | --country | Value required if used, allows user to select all groups from a suspected country of origin. Options are China, Russia, DPRK, Iran, and Other |
| -i | --industry | Value required if used, allows user to select adversaries targeting one of 20 different industry sectors. |
| -a | --adversary | Value required if used, allows user to select specific adversary groups recognized by MITRE. User can enter either the MITRE name or one of their aliases. |
| -m | --malware | Value required if used, allows user to select specific adversary malware. Only supported on Software TTP Aggregator |
| -s | --shared | Value required if used, allows the user to dictate the number of shared groups/malware sets a technique should have in order to be consider relevant. For example, if the user enters '3' then only techniques shared between at least 3 groups/selected malware sets will be shown. |

When running Group TTP Aggregator manually it will ask you for a list of group names, followed by "end". You can use other group names besides the "official" ones used by MITRE. For example, MITRE uses the name APT29, while other organizations might refer to them as "Cozy Bear". Since MITRE recognizes "Cozy Bear" as an associated group, if a user enters this name it will automatically change the user input to "APT29". 

When running Software TTP Aggregator manually you can bulk add groups the same way as in Group TTP Aggregator, or add groups individually. The script will then take all the unique software sets from the selected groups use and add them to the software list for later comparison. This means that if you select a country, all software/malware used by groups suspected to be from that country will be added for analysis. It will then ask you for a list of software names, followed by "end". You can use other software names besides the "official" ones used by MITRE. For example, MITRE uses the name "ASPXSpy", while other organizations might refer to that software as "ASPXTool". Since MITRE recognizes "ASPXTool" as an associated software name, if a user enters this it will automatically change the user input to "ASPXSpy".

After entering all the groups/software a user wants to compare, the user will be asked to choose the minimum number of groups or software sets a TTP needs to be shared between in other to be recorded. So if a user has entered 5 groups or software and selects "3" all TTPs that are shared between 3+ of the selected items will be shown and saved for later analysis.

After all (if any) of the common TTPs are identified, a user can run two different operations. 
1. Show the number of TTPs a given data source could potentially be used to find. For example, if there were 3 TTPs that met the user's previous criteria, this would show the data sources that could potentially identify 3/3 TTPs, then 2/3 TTPs, and so on.
2. Print each shared TTP, then print all of the data sources that could be used to potentially identify that particular TTP. If there are many shared TTPs, this could result in a large amount of text being printed to the command line. This is also the default option used when a user passes commandline arguments to either of the aggregation scripts. 

Assuming a user is not passing commandline arguments, they can go back and forth between these options until they are ready to quit the script, at which point they can select option 3. This exits the case statement and ends the script.

### Group Lists (Current as of July 2020)

**BY COUNTRY**

| COUNTRY | GROUPS |
| ------ | ------ |
| CHINA | admin@338, APT1, APT12, APT16, APT17, APT18, APT19, APT3, APT30, APT41, Axiom, BronzeButler, Deep Panda, Elderwood, Ke3chang, menuPass, Moafee, Mofang, Naikon, Night Dragon, PittyTiger, Putter Panda, Rocke, Soft Cell, Suckfly, TA459, Threat Group-3390, Winnti Group |
| RUSSIA | APT28, APT29, Dragonfly2.0, Sandworm Team, Temp.Veles, Turla |
| DPRK | APT37, APT38, Kimsuky, Lazarus Group, Stolen Pencil |
| IRAN | APT33, APT39, Charming Kitten, Cleaver, CopyKittens, Group 5, Leafminer, Magic Hound, MuddyWater, Oilrig |
| OTHER | APT-C-36, APT32, BlackOasis, DarkCaracal, SilverTerrier |

**BY SECTOR**

| INDUSTRY | GROUPS |
| ------ | ------ |
| Finance | admin@338, APT-C-36, APT19, APT38, Carbanak, Cobalt Group, DarkVishnya, Deep Panda, FIN4, GCMAN, Oilrig, RTM, Silence, Sharpshooter |
| High Tech | APT12, APT17, APT18, APT19, APT41, Elderwood, Magic Hound, menuPass, SilverTerrier, Threat Group-3390, Tropic Trooper |
| Media | APT12, BlackOasis, Charming Kitten, Sandworm Team |
| Government | APT12, APT17, APT18, APT28, APT29, APT-C-36, DarkHydrus, Deep Panda, Dragonfly2.0, Gallmaker, Gorgon Group, Inception, Ke3chang, Leafminer, Leviathan, Lotus Blossom, Magic Hound, menuPass, Mofang, Molrats, MuddyWater, Oilrig, Patchwork, Sandworm Team, Sowbug, Threat Group-3390, Tropic Trooper, Turla, Windshift |
| Defense | APT17, APT19, Deep Panda, Dragonfly, Elderwood, Gallmaker, Leviathan, menuPass, Sharpshooter, Threat Group-3390, Thrip, Turla |
| NGOs | APT17, APT18, Charming Kitten, Elderwood, Honeybee, Scarlet Mimic |
| Law Firms | APT17, APT19 |
| Medical/Healthcare | APT18, APT41, Deep Panda, FIN4, menuPass, Orangeworm, Tropic Trooper, Whitefly |
| Energy | APT19, APT33, Dragonfly, Magic Hound, Oilrig, Sandworm Team, Sharpshooter, Threat Group-3390 |
| Pharmaceuticals | APT19, FIN4, Turla |
| Telecommunications | APT19, APT39, APT41, Deep Panda, MuddyWater, Oilrig, Soft Cell, Thrip |
| Education | APT19, Charming Kitten, DarkHydrus, Leviathan, menuPass, SilverTerrier, Stolen Pencil, Turla |
| Manufacturing | APT18, APT19, APT-C-36, Leviathan, menuPass, SilverTerrier, Threat Group-3390 |
| Aviation | APT33, Dragonfly, menuPass, Threat Group-3390 |
| Hospitality | DarkHotel, FIN5, FIN6, FIN7, FIN8 |
| Critical Infrastructure | Dragonfly, Dragonfly2.0, Mofang, Sandworm Team, Temp.Veles, Windshift |
| Oil | APT-C-36, Ke3chang, MuddyWater |
| Video Games | APT41, FIN5, Winnti Group |
| Diplomatic | Patchwork, Write, Turla |
| International | APT16, APT3, APT32, APT33, APT37, APT38, APT41, APT-C-36, Blacktech, BronzeButler, Charming Kitten, Cobalt Group, CopyKittens, DarkHydrus, DarkVishnya, DragonOK, Dust Storm, Gallmaker, Gamaredon Group, Gorgon Group, Group 5, Honeybee, Inception, Kimsuky, Leviathan, Lotus Blossom, Machete, Magic Hound, menuPass, Mofang, MuddyWater, Naikon, Neodymium, Oilrig, Orangeworm, Platinum, Promethium, Rancor, RTM, Sandworm Team, Silence, Sowbug, Stealth Falcon, Strider, TA459, Taidoor, The White Company, Thrip, Tropic Trooper, Turla, Windshift, Whitefly, Write |

### KNOWN BUGS

**Group_TTP_Aggregator_v1.4.0.py**:
Any groups that only have PRE-ATT&CK techniques will show up on the adversary
list, but will be dropped when checked against the Excel file. Looks like the
code to pull data from MITRE does not pull PRE-ATT&CK techniques, so any groups
that only have those techniques are not captured.

**Software_TTP_Aggregator_v1.4.0.py**:
There are some software sets that are not tied to specific threat actors, especially
Android malware. Just something to be aware of if you are trying to add software
by group/sector/country.

### Changelog
| Version | Changes | 
| ------ | ------ |
| v1.4.0 | Added commandline argument support |
| v1.3.0 | Initial release |

### Future Updates
1. Clean up text output
2. ASCII art I guess?? ¯\(°_o)/¯
3. GUI (long-term goal)

### Questions, Comments, or Feedback?
Feel free to reach out to me on Twitter at [@aptgetKubert](https://twitter.com/aptgetKubert)