# Author: aptgetKubert
# Date: December 05, 2020
# Purpose: Offline aggregation of adversary software and mapping to data sources that can be monitored and potentially ingested into a SIEM  
# pip install attackcti

import collections
import getopt, sys

# Get all command-line args
allCmdArgs = sys.argv

# Remove first arg so users cannot see path to script if they try to return sys.argv[0]
userCmdArgs = allCmdArgs[1:]

# Define short and long command values. Allow a user to get a help message or specify a specific suspect country, target industry, or adversary
shortOptions = "hc:i:a:m:s:"
longOptions = ["help", "country=", "industry=", "adversary=","malware=","shared="]

# Try-Catch statement to cover errors. Include a custom error message to avoid leaking extra details in the default error output
try:
    args, values = getopt.getopt(userCmdArgs, shortOptions, longOptions)
except:
    print("You are trying to use an unrecognized argument. Please only use -h, -c, -i, -a, -m, and/or -s")
    sys.exit(2)

# Check valid arguments and run calculations
cmdlineUserArgsSet = False
for currentArgs, currentValue in args:
    if currentArgs in ("-h", "--help"):
        print()
        print("-h (--help): Prints this help message. Allows user to find shared adversary techniques and the data sources needed to hunt for those techniques. If one of -c, -i, or -a flags are used then the script will not allow users to manually enter any other selections.")
        print()
        print("-c (--country): Value required, allows user to select suspected adversary country of origin. If you want to select multiple countries seperate each with a ','. Options are China, Russia, DPRK, Iran, and Other")
        print(" EX: -c China,Russia")
        print()
        print("-i (-industry): Value required, allows user to select adversaries targeting one of 20 different industry sectors. If you want to select multiple industries seperate each with a ','. If the industry is two words (i.e. High Tech) be sure to surround it in quotes. Options are Finance, High Tech, Media, Government, Defense, NGOs, Law Firms, Medical/Healthcare, Energy, Pharmaceuticals, Telecommunications, Education, Manufacturing, Aviation, Hospitality, Critical Infrastructure, Oil, Video Games, Diplomatic, and International")
        print("EX: -i Finance,'High Tech'")
        print()
        print("-a (--adversary): Value required, allows user to select specific adversary groups recognized by MITRE. User can enter either the MITRE name or one of their aliases. If you want to select multiple adversaries seperate each with a ','.")
        print("EX: -a APT33, Oilrig")
        print()
        print("-m (--malware): Value required, allows user to select specific adversary malware or local software that can be misused by the attacker. User must use the name recognized by MITRE. If you want to select multiple adversares seperate each with a ','.")
        print("EX: -m Empire, net")
        print()
        print("-s (--shared): Value required, allows the user to dictate the number of shared malware sets a technique should have in order to be consider relevant. For example, if the user enters '3' then only techniques shared between at least 3 selected malware sets will be shown. This will also cause the data source calculation to default to the second option, which prints all the data sources a defender could use to hunt for each of the selected techniques.")
        print("EX: -s 3")
    elif currentArgs in ("-c", "--country"):
        cmdlineCountry = currentValue
        cmdlineUserArgsSet = True
    elif currentArgs in ("-i", "--industry"):
        cmdlineIndustry = currentValue
        cmdlineUserArgsSet = True
    elif currentArgs in ("-a", "--adversary"):
        cmdlineAdversary = currentValue
        cmdlineUserArgsSet = True
    elif currentArgs in ("-m", "--malware"):
        cmdlineMalware = currentValue
        cmdlineUserArgsSet = True
    elif currentArgs in ("-s", "--shared"):
        cmdlineShared = currentValue
        # Make sure the user entered an actual number
        try:
            cmdlineShared = int(cmdlineShared)
        except ValueError:
            print("This is not a valid number, please try again.\n")
            sys.exit(2)
        cmdlineUserArgsSet = True

# Open generated CSVs to save values for later calculations
software = open("MITRE_software_output.csv","r") 
dataSources = open("MITRE_data_sources_output.csv","r")
groupSoftware = open("MITRE_software_to_groups_output.csv","r")

# Create class "Software" that will store the TTPs that every malware/LOtL software uses for comparison later
class Software:
    'Common class for all threat actor software'
    # As class variables, if these are directly changed, they wil be changed for ALL objects. See below for methods to change variables for each object
    softwareCount = 0 # This will be used to track number of software that a user has requested to be compared
    softwarePosition = 0 # Used to save position of group in "group_output.csv". This allows us to tie specific TTPs to a particular group

    def __init__(self, name):
        self.name = name
        self.TTPs = [] # Array for known TTPs to be stored for later comparison
        Software.softwareCount += 1 # Update total number of group objects each time one is created

    def showCount(self):
        print("Total software requested by user: %d" % Software.softwareCount)
    
    def showTTPs(self):
        print(self.TTPs)

    def setSoftwarePosition(self, pos):
        self.softwarePosition = pos
    
    def addSoftwareTTPs(self, TTPPair):
        self.TTPs.append(TTPPair)

# Createclass "TTP" that will store the ID number, name, and count in terms of use based on the user slected adversary software
# Also will store data sources that can be used to discover this particular TTP.
class TTP:
    'Common class for all TTPs'

    def __init__(self, name, number, count):
        self.name = name # I.e. Account Access Removal
        self.number = number # I.e. T1531
        self.TTPCount = count # Tracks number of software selected by the user which use this TTP
        self.dataSources = [] # Array to hold positions, which can be later matached to specific Data Sources
    
    def addDataSource(self, dataSourcePosition):
        self.dataSources.append(dataSourcePosition)

# Switch case statements allowing the user to conduct various operations with the common TTPs and data sources that can be used to find them
# Options are: List # of TTPs each data source can find, show which data sources can be used to find each TTP, and exit
def dataSourceOperations(TTPObjectList,dataSourceNames,numOfSelectedSoftware,wasCmdlineSet):
    #If wasCmdlineSet is true, it means the user used the -s/--shared flag and the data sources calculation must default to option 2
    if wasCmdlineSet == True:
        userSubmittedOperation = 2
    else:
        print("Enter '1' if you want to see the total number of common TTPs each data source can be used to find")
        print("Enter '2' if you want to see each common TTP and the data source(s) that can be used to find them")
        print("Enter '3' to exit")
        userOperationIsGood = False
        while userOperationIsGood == False:
            userSubmittedOperation = input("Please enter your choice: ")
            print()
            try:
                userSubmittedOperation = int(userSubmittedOperation)
                if userSubmittedOperation >= 1 and userSubmittedOperation <= 3:
                    userOperationIsGood = True
                else:
                    print("That is not valid a number, try something between 1 and 3 (inclusive)\n")
            except ValueError:
                print("This is not a valid number, please try again.\n")
    
    # List # of shared TTPs each data source can be used to find
    # First create array with the data sorce positions, this will make it easier to convert them to to data source names
    if userSubmittedOperation == 1:
        totalDataSources = []
        for v in range(0,len(TTPObjectList)):
            for w in range(0,len(TTPObjectList[v].dataSources)):
                totalDataSources.append(TTPObjectList[v].dataSources[w])

        # Match positions to actual data source names
        for aa in range(0,len(totalDataSources)):
            for bb in range(4,len(dataSourceNames)):
                if totalDataSources[aa] == bb:
                    totalDataSources[aa] = dataSourceNames[bb]
        
        # Creates a dictionary that tracks the number of times a data source is seen among all of the shared TTPs
        dataSourcesCountDict = {cc:totalDataSources.count(cc) for cc in totalDataSources}

        # Loop through the TTP objects and print in decending order the data sources and number of shared TTPs they can potentially find
        for dd in range(len(TTPObjectList),0,-1):
            for dataSource, dataSourceCount in dataSourcesCountDict.items():
                if dataSourceCount == dd and dataSource != ',':
                    print("%s can be used to potentially find %s/%d shared TTPs\n" % (dataSource,dataSourceCount,len(TTPObjectList)))
        return(False)
    
    # Print each shared TTP and the data sources that can be used to find it. MAY RESULT IN A LOT OF TEXT
    elif userSubmittedOperation == 2:
        # Loop through all TTP objects, and loop through all their data sources to convert position numbers to data source names
        for ee in range(0,len(TTPObjectList)):
            for ff in range(0,len(TTPObjectList[ee].dataSources)):
                for gg in range(4,len(dataSourceNames)):
                    if TTPObjectList[ee].dataSources[ff] == gg:
                        TTPObjectList[ee].dataSources[ff] = dataSourceNames[gg]
        
        # Print each TTP name followed by their associated data sources
        for hh in range(0,len(TTPObjectList)):
            print("%s can be potentially found using the following data source(s)" % TTPObjectList[hh].name)
            for ii in range(0,len(TTPObjectList[hh].dataSources)):
                print("\t%s" % TTPObjectList[hh].dataSources[ii])
        #If cmdline -s/--shared was used, exit after single execution
        if wasCmdlineSet == True:
            return(True)
        else:
            return(False)

    # Exit data source operations, and exits program
    elif userSubmittedOperation == 3:
        return(True)

# This function allows for a user to add a group of adversary groups based on country or target industry
# This function is almost identical to "bulkGroupAdd" in "Group_TTP_Aggregator.py", but once you have a list of group, this script will create a list of the unique software sets shared between them.
def bulkGroupAdd(countryArray,industryArray,groupAliasArray,groupArray):
    doneAddingByCountry = False
    addedCountryCode = []
    while doneAddingByCountry == False:
        # If cmdlineUserArgsSet is True then it means a user set variables from the command line. It will then skip the next portion that would require user input
        if cmdlineUserArgsSet == True:
            doneAddingByCountry = True
            # Try-Catch block to check if the -c/--country flag was used. If so, take cmdline input for calculations
            try:
                cmdlineCountry
                cmdlineCountryArray = cmdlineCountry.split(",")
                for cmdlineCountryArrayCounter in range(0,len(cmdlineCountryArray)):
                    for userCountry in cmdlineCountryArray:
                        normalizedCountry = userCountry.lower()
                        for ab in range(0,5):
                            if normalizedCountry == countryArray[ab][0].lower():
                                for groupCounter in range(1,len(countryArray[ab])):
                                    groupArray.append(countryArray[ab][groupCounter])
            except NameError:
                pass
        else:
            print("You may now select adversary groups in bulk based off suspected country of origin or target industry.")
            print("Select 1 for %s, 2 for %s, 3 for %s, 4 for %s, 5 for %s, and 6 to skip to target industry." % (countryArray[0][0],countryArray[1][0],countryArray[2][0],countryArray[3][0],countryArray[4][0]))
            userSubmittedCountry = input("Number: ")
            # Make sure the user input is a number
            try:
                userSubmittedCountry = int(userSubmittedCountry)
                # Make sure the user input is a valid number and hasn't been selected already to eliminate possible duplicates
                if userSubmittedCountry <= 0 or userSubmittedCountry >= 7:
                    print("That is not valid a number, try something between 1 and 6 (inclusive)\n")
                elif userSubmittedCountry in addedCountryCode:
                    print("You have already added that country!\n")
                elif userSubmittedCountry == 6:
                    doneAddingByCountry = True
                else:
                    addedCountryCode.append(userSubmittedCountry)
                    for jj in range(1,len(countryArray[userSubmittedCountry-1])):
                        groupArray.append(countryArray[userSubmittedCountry-1][jj])
                    print("You have added groups suspected to be from: %s\n" % countryArray[userSubmittedCountry-1][0])
            except ValueError:
                print("This is not a valid number, please try again.\n")
    doneAddingByIndustry = False
    addedIndustryCode = []
    while doneAddingByIndustry == False:
        # If cmdlineUserArgsSet is True then it means a user set variables from the command line. It will then skip the next portion that would require user input
        if cmdlineUserArgsSet == True:
            doneAddingByIndustry = True
            # Try-Catch block to check if the -i/--industry flag was used. If so, take cmdline input for calculations
            try:
                cmdlineIndustry
                cmdlineIndustryArray = cmdlineIndustry.split(",")
                for cmdlineIndustryArrayCounter in range(0,len(cmdlineIndustryArray)):
                    for userIndustry in cmdlineIndustryArray:
                        normalizedIndustry = userIndustry.lower()
                        for ab in range(0,20):
                            if normalizedIndustry == industryArray[ab][0].lower():
                                for groupCounter in range(1,len(industryArray[ab])):
                                    groupArray.append(industryArray[ab][groupCounter])
            except NameError:
                pass
        else:
            print("You may now select adversary groups in bulk based off target industry")
            print("Select 1 for %s, 2 for %s, 3 for %s, 4 for %s, 5 for %s," % (industryArray[0][0],industryArray[1][0],industryArray[2][0],industryArray[3][0],industryArray[4][0]))
            print("Select 6 for %s, 7 for %s, 8 for %s, 9 for %s, 10 for %s," % (industryArray[5][0],industryArray[6][0],industryArray[7][0],industryArray[8][0],industryArray[9][0]))
            print("Select 11 for %s, 12 for %s, 13 for %s, 14 for %s, 15 for %s," % (industryArray[10][0],industryArray[11][0],industryArray[12][0],industryArray[13][0],industryArray[14][0]))
            print("Select 16 for %s, 17 for %s, 18 for %s, 19 for %s, 20 for %s" % (industryArray[15][0],industryArray[16][0],industryArray[17][0],industryArray[18][0],industryArray[19][0]))
            print("If you would like to skip this step, enter 0 ")
            userSubmittedIndustry = input("Number: ")
            try:
                userSubmittedIndustry = int(userSubmittedIndustry)
                # Make sure the user input is a valid number and hasn't been selected already to eliminate possible duplicates
                if userSubmittedIndustry < 0 or userSubmittedIndustry > 20:
                    print("That is not valid a number, try something between 0 and 20 (inclusive)\n")
                elif userSubmittedIndustry in addedIndustryCode:
                    print("You have already added that industry!\n")
                elif userSubmittedIndustry == 0:
                    doneAddingByIndustry = True
                else:
                    addedIndustryCode.append(userSubmittedIndustry)
                    for ll in range(1,len(industryArray[userSubmittedIndustry-1])):
                        groupArray.append(industryArray[userSubmittedIndustry-1][ll])
                    if userSubmittedIndustry == 20:
                        print("You have added groups that have targeted non-US countries\n")
                    else:
                        print("You have added groups that have targeted the %s industry\n" % industryArray[userSubmittedIndustry-1][0])
            except ValueError:
                print("This is not a valid number, please try again.\n")

    #Save first line of groups mapped to their software sets, save to a list
    firstGroupSoftwareRow = next(groupSoftware)
    nameGroupSoftwareArray = firstGroupSoftwareRow.split(",")

    # Normalize values for later checking against user input
    editedGroupSoftwareNameArray = []
    for n in range(len(nameGroupSoftwareArray)):
        editedGroupSoftwareNameArray.append(nameGroupSoftwareArray[n].lower())
        # The CSV file adds a '\n' character to the last string in nameGroupSoftwareArray. This will cause it to not match should someone want to compare that group. 
        # This if statement checks if we are at the end of the array, and if so to remove the last character in the string, which will be '\n'.
        if n == len(nameGroupSoftwareArray)-1:
            editedGroupSoftwareNameArray[n] = editedGroupSoftwareNameArray[n][:-1]

    # User can now add individual groups that will then be used to find the software sets each group uses
    groupUserContinue = False
    # If cmdlineUserArgsSet is True then it means a user set variables from the command line. It will then skip the next portion that would require user input
    if cmdlineUserArgsSet == True:
        # Try-Catch block to check if the -a/--adversary flag was used. If so, take cmdline input for calculations
        try:
            cmdlineAdversary
            cmdlineAdversaryArray = cmdlineAdversary.split(",")
            for cmdlineAdversaryArrayCounter in range(0,len(cmdlineAdversaryArray)):
                for userAdversary in cmdlineAdversaryArray:
                    normalizedAdversary = userAdversary.lower()
                    # Check if user entered adversary is an associated group name for a different threat actor. If so, change it to the MITRE recognized name. 
                    for r in range(0,len(groupAliasArray)):
                        for s in range(1,len(groupAliasArray[r])):
                            if normalizedAdversary == groupAliasArray[r][s]:
                                normalizedAdversary = groupAliasArray[r][0]
                    groupArray.append(normalizedAdversary)
            # Remove any duplicate groups
            groupArray = removeDuplicates(groupArray)
        except NameError:
            pass
    else:
        print("Please enter the name of the APT groups you would like compare software sets of. For groups such as APT## do not add a space between 'APT' and '##'. When you are done type 'end'.")
        while groupUserContinue == False:
            userSubmittedGroupName = input("Enter APT group name: ")
            cleaningUserSubmittedGroupName = userSubmittedGroupName.lstrip() #Remove leading spaces
            cleaningUserSubmittedGroupName = cleaningUserSubmittedGroupName.rstrip() #Remove trailing spaces
            cleaningUserSubmittedGroupName = cleaningUserSubmittedGroupName.lower() #Convert string to lowercase

            userInputIsSanitized = False
    
            #Check if the user is done entering names
            if cleaningUserSubmittedGroupName == 'end':
                groupUserContinue = True
                userInputIsSanitized = True
    
            # Check if user input is already in the group name array
            for qq in range(2,len(editedGroupSoftwareNameArray)):
                if cleaningUserSubmittedGroupName == editedGroupSoftwareNameArray[qq]:
                    userInputIsSanitized = True
                    break
    
            # Check if a user entered a name of an associated group, if so change it to the MITRE group name.
            if userInputIsSanitized == False:
                possibleAssociatedGroup = cleaningUserSubmittedGroupName
                for rr in range(0,len(groupAliasArray)):
                    for ss in range(1,len(groupAliasArray[rr])):
                        if cleaningUserSubmittedGroupName == groupAliasArray[rr][ss]:
                            cleaningUserSubmittedGroupName = groupAliasArray[rr][0]
                            userInputIsSanitized = True
    
                # If the user entered an associated group name, tell the user that the name has been switched
                if possibleAssociatedGroup != cleaningUserSubmittedGroupName:
                    print("You originally entered %s, which is an associated group of %s." % (possibleAssociatedGroup,cleaningUserSubmittedGroupName))
                    print("Your entry has been changed to %s in order to compare TTPs" % cleaningUserSubmittedGroupName)
    
            # If userInputIsSanitized is still false at this point, it means that the group name was not found in the MITRE list, nor one of the associated groups.
            if userInputIsSanitized == False:
                print("Your input of '%s' does not seem to match any group or associated group names. Please try again" % cleaningUserSubmittedGroupName)
            else:
                #Save input to array of "scrubbed" group names, increment number of valid entries
                if cleaningUserSubmittedGroupName != 'end':
                    groupArray.append(cleaningUserSubmittedGroupName)

    #Get array position of user requested group(s), then save any software the groups use to bulkSoftwareList
    bulkSoftwareList = []
    for userGroup in groupArray:
        for aa in range(0,len(editedGroupSoftwareNameArray)):
            if userGroup == editedGroupSoftwareNameArray[aa]:
                # aa==2 if group name is windshift
                groupSoftware.seek(1) # Reset to top of file to reread lines for new software
                for line in groupSoftware:
                    groupSoftwareArray = line.split(",")
                    # The below if/else statement is used to avoid an index out of bounds exception if the user has selected the last group in the groupSoftwareArray
                    if aa == len(groupSoftwareArray):
                        groupPosition = aa-1
                    else:
                        groupPosition = aa
                    if groupSoftwareArray[groupPosition] == '1.0' or groupSoftwareArray[groupPosition] == '1.0\n': # The '1.0\n' is needed becuase a '\n' character is added to the last entry in each line of the CSV file. This means if the user selects the last group, any software it uses will show as '1.0\n', not the usual '1.0'
                        bulkSoftwareList.append(groupSoftwareArray[1])
    #print(bulkSoftwareList)
    return bulkSoftwareList

# Function to remove any duplicates from a given array
def removeDuplicates(originalArray):
    duplicatesRemoved = set()
    uniqGroups = []
    for mm in originalArray:
        if mm not in duplicatesRemoved:
            uniqGroups.append(mm)
            duplicatesRemoved.add(mm)
    return uniqGroups

#lowercase all software in case the user wants to remove any
def lowerArray(passedArray):
    lowercaseName = []
    for name in passedArray:
        lowercaseName.append(name.lower())
    return lowercaseName   

#Save first line of Software Output CSV with all software names, save to a list
firstSoftwareRow = next(software)
nameArray = firstSoftwareRow.split(",")

# Normalize values for later checking against user input
editedNameArray = []
for n in range(len(nameArray)):
    editedNameArray.append(nameArray[n].lower())
    # The CSV file adds a '\n' character to the last string in nameArray. This will cause it to not match should someone want to compare that group. 
    # This if statement checks if we are at the end of the array, and if so to remove the last character in the string, which will be '\n'.
    if n == len(nameArray)-1:
        editedNameArray[n] = editedNameArray[n][:-1]

# Save first line of Data Sources Output CSV, with all the MITRE ID'd data sources, save to a list
firstDataSourcesRow = next(dataSources)
dataSourceNameArray = firstDataSourcesRow.split(",")
# Take the last entry in the "dataSourceNameArray" and remove the newline character
dataSourceNameArray[len(dataSourceNameArray)-1] = dataSourceNameArray[len(dataSourceNameArray)-1][:-1]

# Array of arrays for "Associated software", formerly known as "Aliases". This helps check user input against other associated software names.
# Currently I make this manually. I'm sure there is a way to automate this, just havent done it yet.
associatedSoftwareArray = [['advstoreshell','azzy','eviltoss','netui','sedreco'],['arp','arp.exe'],['aspxspy','aspxtool'],['at','at.exe'],['auditcred','roptimizer'],['backdoor.oldrea','havex'],['backspace','lecna'],['bankshot','trojan manuscript'],['blackenergy','black energy'],['bread','joker'],['bubblewrap','backdoor.apt.fakewinhttphelper'],['bundlore','osx.bundlore'],['carbanak','anunak'],['certutil','certutil.exe'],['chches','scorpion','haymaker'],['chopstick','backdoor.sofacyx','splm','xagent','x-agent','webhp'],['cloudduke','minidionis','cloudlook'],['cmd','cmd.exe'],['concipit1248','corona updates'],['coreshell','sofacy','sourface'],['corona updates','wabi music','concipit1248'],['cosmicduke','tinybaron','botgenstudios','nemesisgemina'],['cozycar','cozyduke','cozybear','cozer','euroapt'],['crimson','msil','msil/crimson'],['darkcomet','darkkomet','fynloski','krademok','fynlos'],['daserf','muirim','nioupale'],['derusbi','photo'],['dok','retefe'],['downdelph','delphacy'],['dridex','bugat v5'],['dsquery','dsquery.exe'],['dustysky','ned worm'],['dyre','dyzap','dyreza'],['elise','bkdr_esile','page'],['emotet','geodo'],['empire','empyre','powershell empire'],['epic','tavdig','wipbot','worldcupsec','tadjmakhal'],['esentutl','esentutl.exe'],['exodus','exodus one','exodus two'],['felixroot','greyenergy mini'],['finfisher','finspy'],['flame','flamer','skywiper'],['ftp','ftp.exe'],['gazer','whitebear'],['glooxmail','trojan.gtalk'],['gooligan','ghost push'],['hammertoss','hammerduke','netduke'],['hdoor','custom hdoor'],['htran','huc packet transmit tool'],['httpbrowser','token control','httpdump'],['hydraq','aurora','9002 rat'],['ikitten','osx/macdownloader'],['ipconfig','ipconfig.exe'],['jhuhugit','trojan.sofacy','seduploader','jkeyskw','sednit','gamefish','sofacycarberp'],['jrat','jsocket','alienspy','frutas','sockrat','unrecom','jfrutas','adwind','jbifrost','trojan.maljava'],['keydnap','osx/keydnap'],['lurid','enfal'],['miner-c','mal/miner-c','photominer'],['more_eggs','terra loader','spicyomelette'],['nbtstat','nbtstat.exe'],['net','net.exe'],['net crawler','netc'],['netsh','netsh.exe'],['netstat','netstat.exe'],['nidiran','backdoor.nidiran'],['njrat','njw0rm','lv','bladabindi'],['notpetya','goldeneye','petrwrap','nyetya'],['oldbait','sasfis'],['orz','airbreak'],['osx/shlayer','crossrider'],['p2p zeus','peer-to-peer zeus','gameover zeus'],['pegasus for android','chrysaor'],['plugx','destroyrat','sogu','kaba','korplug'],['poisonivy','poison ivy','darkmoon'],['power loader','win32/agent.uaw'],['powersource','dnsmessenger'],['powerstats','powermud'],['punchbuggy','shelltea'],['punchtrack','psvc'],['quasarrat','xrat'],['rawpos','fiendcry','duebrew','driftwood'],['redleaves','bugjuice'],['reg','reg.exe'],['remsec','backdoor.remsec','projectsauron'],['route','route.exe'],['rtm','redaman'],['sakula','sakurel','viper'],['samsam','samas'],['schtasks','schtasks.exe'],['seaduke','seadaddy','seadesk'],['shamoon','disttrack'],['shotput','backdoor.apt.cookiecutter','pirpi'],['smoke loader','dofoil'],['stonedrill','dropshot'],['textmate','dnsmessenger'],['trickbot','totbrick','tspy_trickload'],['uppercut','anel'],['ursnif','gozi-isfb','pe_ursnif','dreambot'],['usbstealer','usb stealer','win32/usbstealer'],['viceleaker','triout'],['wannacry','wanacry','wanacrypt','wanacrypt0r','wcry'],['windows credential editor','wce'],['xagentosx','osx.sofacy'],['xtunnel','trojan.shunnael','x-tunnel','xaps'],['zebrocy','zekapab'],['zeroaccess','trojan.zeroaccess'],['zxshell','sensocode']]

# Save software name the user wants to view
vettedSoftwareList = []

# Begin bulk add of groups
# These arrays are currently made manually. To automate it would require a script to look for certain keywords in the MITRE group descriptions. Probably possible, just haven't done it yet
groupByCountryArray = [['China','admin@338','apt1','apt12','apt16','apt17','apt18','apt19','apt3','apt30','apt41','axiom','bronzebutler','deep panda','elderwood','ke3chang','menupass','moafee','mofang','naikon','night dragon','pittytiger','putter panda','rocke','soft cell','suckfly','ta459','threat group-3390','winnti group'],['Russia','apt28','apt29','dragonfly 2.0','sandworm team','temp.veles','turla'],['DPRK','apt37','apt38','kimsuky','lazarus group','stolen pencil'],['Iran','apt33','apt39','charming kitten','cleaver','copykittens','group 5','leafminer','magic hound','muddywater','oilrig'],['Other','apt-c-36','apt32','blackoasis','dark caracal','silverterrier']]
groupsByIndustryArray = [['Finance','admin@338','apt-c-36','apt19','apt38','carbanak','cobalt group','darkvishnya','deep panda','fin4','gcman','oilrig','rtm','silence','sharpshooter'],['High Tech','apt12','apt17','apt18','apt19','apt41','elderwood','magic hound','menupass','silverterrier','threat group-3390','tropic trooper'],['Media','apt12','blackoasis','charming kitten','sandworm team'],['Government','apt12','apt17','apt18','apt28','apt29','apt-c-36','darkhydrus','deep panda','dragonfly 2.0','gallmaker','gorgon group','inception','ke3chang','leafminer','leviathan','lotus blossom','magic hound','menupass','mofang','molerats','muddywater','oilrig','patchwork','sandworm team','sowbug','threat group-3390','tropic trooper','turla','windshift'],['Defense','apt17','apt19','deep panda','dragonfly','elderwood','gallmaker','leviathan','menupass','sharpshooter','threat group-3390','thrip','turla'],['NGOs','apt17','apt18','charming kitten','elderwood','honeybee','scarlet mimic'],['Law Firms','apt17','apt19'],['Medical/Healthcare','apt18','apt41','deep panda','fin4','menupass','orangeworm','tropic trooper','whitefly'],['Energy','apt19','apt33','dragonfly','magic hound','oilrig','sandworm team','sharpshooter','threat group-3390'],['Pharmaceuticals','apt19','fin4','turla'],['Telecommunications','apt19','apt39','apt41','deep panda','muddywater','oilrig','soft cell','thrip'],['Education','apt19','charming kitten','darkhydrus','leviathan','menupass','silverterrier','stolen pencil','turla'],['Manufacturing','apt18','apt19','apt-c-36','leviathan','menupass','silverterrier','threat group-3390'],['Aviation','apt33','dragonfly','menupass','threat group-3390'],['Hospitality','darkhotel','fin5','fin6','fin7','fin8'],['Critical Infrastructure','dragonfly','dragonfly 2.0','mofang','sandworm team','temp.veles','windshift'],['Oil','apt-c-36','ke3chang','muddywater'],['Video Games','apt41','fin5','winnti group'],['Diplomatic','patchwork','write','turla'],['International','apt16','apt3','apt32','apt33','apt37','apt38','apt41','apt-c-36','blacktech','bronzebutler','charming kitten','cobalt group','copykittens','darkhydrus','darkvishnya','dragonok','dust storm','gallmaker','gamaredon group','gorgon group','group 5','honeybee','inception','kimsuky','leviathan','lotus blossom','machete','magic hound','menupass','mofang','muddywater','naikon','neodymium','oilrig','orangeworm','platinum','promethium','rancor','rtm','sandworm team','silence','sowbug','stealth falcon','strider','ta459','taidoor','the white company','thrip','tropic trooper','turla','windshift','whitefly','write']]
associatedGroupArray = [['admin@338','temper panda','admin338','team338','338 team'],['apt-c-36','blind eagle'],['apt1','comment crew','comment group','comment panda'],['apt12','ixeshe','dyncalc','numbered panda','dnscalc'],['apt17','deputy dog'],['apt18','tg-0416','dynamite panda','threat group-0416'],['apt19','codoso','c0d0so0','codoso team','sunshop group'],['apt28','snakemackerel','swallowtail','group 74','sednit','sofacy','pawn storm','fancy bear','strontium','tsar team','threat group-4127','tg-4127'],['apt29','yttrium','the dukes','cozy bear','cozyduke'],['apt3','gothic panda','pirpi','ups team','buckeye','threat group-0110','tg-0110'],['apt32','sealotus','oceanlotus','apt-c-00'],['apt33','elfin'],['apt37','scarcruft','reaper','group123','temp.reaper'],['apt39','chafer'],['axiom','group72'],['bronze butler','redbaldknight','tick'],['carbanak','anunak','carbon spider'],['cleaver','threat group 2889','tg-2889'],['cobalt group','cobalt gang','cobalt spider'],['deep panda','shell crew','webmasters','kungfu kittens','pinkpanther','black vine'],['dragonfly','energetic bear'],['dragonfly 2.0','berserk bear'],['elderwood','elderwood gang','beijing group','sneaky panda'],['fin6','itg08'],['inception','inception framework','cloud atlas'],['ke3chang','apt15','mirage','vixen panda','gref','playful dragon','royalapt'],['kimsuky','velvet chollima'],['lazarus group','hidden cobra','guardians of peace','zinc','nickel academy'],['leafminer','raspite'],['leviathan','temp.jumper','apt40','temp.periscope'],['lotus blossom','dragonfish','spring dragon'],['machete','el machete'],['magic hound','rocket kitten','operation saffron rose','ajax security team','operation woolen-goldfish','newscaster','cobalt gypsy','apt35'],['menupass','stone panda','apt10','red apollo','cvnx','hogfish'],['molerats','operation molerats','gaza cybergang'],['muddywater','seedworm','temp.zagros'],['oilrig','irn2','helix kitten','apt34'],['patchwork','dropping elephant','chinastrats','monsoon','operation hangover'],['putter panda','apt2','msupdater'],['sandworm team','quedagh','voodoo bear'],['strider','projectsauron'],['temp.veles','xenotime'],['threat group-1314','tg-1314'],['threat group-3390','tg-3390','emissary panda','bronze union','apt27','iron tiger','luckymouse'],['tropic trooper','keyboy'],['turla','waterbug','whitebear','venomous bear','snake','krypton'],['windshift','bahamut'],['winnti group','blackfly'],['wizard spider','temp.mixmaster','grim spider']]
print()
vettedSoftwareList = bulkGroupAdd(groupByCountryArray,groupsByIndustryArray,associatedGroupArray,vettedSoftwareList)

# Test case software: PoshC2, Empire, MailSniper, NotCompatible, roptimizer, blargblarg
userContinue = False
# If cmdlineUserArgsSet is True then it means a user set variables from the command line. It will then skip the next portion that would require user input
if cmdlineUserArgsSet == True:
    # Try-Catch block to check if the -m/--malware flag was used. If so, take cmdline input for calculations
    try:
        cmdlineMalware
        cmdlineMalwareArray = cmdlineMalware.split(",")
        for cmdlineMalwareArrayCounter in range(0,len(cmdlineMalwareArray)):
            for userMalware in cmdlineMalwareArray:
                normalizedMalware = userMalware.lower()
                # Check if user entered malware is an associated group name for a different malware/software. If so, change it to the MITRE recognized name. 
                for r in range(0,len(associatedSoftwareArray)):
                    for s in range(1,len(associatedSoftwareArray[r])):
                        if normalizedMalware == associatedSoftwareArray[r][s]:
                            normalizedMalware = associatedSoftwareArray[r][0]
                for ab in range(4,len(editedNameArray)):
                    if normalizedMalware == editedNameArray[ab].lower():
                        vettedSoftwareList.append(normalizedMalware)
        # Remove any duplicate groups
        vettedSoftwareList = removeDuplicates(vettedSoftwareList)
    except NameError:
        pass
else:
    print("Please enter the name of the APT software you would like to compare. When you are done type 'end'.")
    while userContinue == False:
        userSubmittedSoftwareName = input("Enter software name: ")
        cleaningUserSubmittedSoftwareName = userSubmittedSoftwareName.lstrip() #Remove leading spaces
        cleaningUserSubmittedSoftwareName = cleaningUserSubmittedSoftwareName.rstrip() #Remove trailing spaces
        cleaningUserSubmittedSoftwareName = cleaningUserSubmittedSoftwareName.lower() #Convert string to lowercase

        userInputIsSanitized = False
    
        #Check if the user is done entering names
        if cleaningUserSubmittedSoftwareName == 'end':
            userContinue = True
            userInputIsSanitized = True
    
        # Check if user input is already in the group name array
        for q in range(4,len(editedNameArray)):
            if cleaningUserSubmittedSoftwareName == editedNameArray[q]:
                userInputIsSanitized = True
                break
    
        # Check if a user entered a name of an associated group, if so change it to the MITRE group name.
        if userInputIsSanitized == False:
            possibleAssociatedSoftware = cleaningUserSubmittedSoftwareName
            for r in range(0,len(associatedSoftwareArray)):
                for s in range(1,len(associatedSoftwareArray[r])):
                    if cleaningUserSubmittedSoftwareName == associatedSoftwareArray[r][s]:
                        cleaningUserSubmittedSoftwareName = associatedSoftwareArray[r][0]
                        userInputIsSanitized = True
    
            # If the user entered an associated group name, tell the user that the name has been switched
            if possibleAssociatedSoftware != cleaningUserSubmittedSoftwareName:
                print("You originally entered %s, which is an associated name for %s." % (possibleAssociatedSoftware,cleaningUserSubmittedSoftwareName))
                print("Your entry has been changed to %s in order to compare TTPs" % cleaningUserSubmittedSoftwareName)
    
        # If userInputIsSanitized is still false at this point, it means that the group name was not found in the MITRE list, nor one of the associated software.
        if userInputIsSanitized == False:
            print("Your input of '%s' does not seem to match any software or associated software names. Please try again" % cleaningUserSubmittedSoftwareName)
        else:
            #Save input to array of "scrubbed" group names, increment number of valid entries
            if cleaningUserSubmittedSoftwareName != 'end':
                vettedSoftwareList.append(cleaningUserSubmittedSoftwareName)

    vettedSoftwareList = removeDuplicates(vettedSoftwareList)

    #lowercase all software in case the user wants to remove any
    vettedSoftwareList= lowerArray(vettedSoftwareList)

    # Allow user to remove any software sets, then print vettedSoftwareList and number of valid software sets
    print("\nHere is your final list of %d valid adversary software sets" % len(vettedSoftwareList))
    print(vettedSoftwareList)
    removingSelection = False
    while removingSelection == False:
        print("Would you like to remove any groups from the list?")
        userRemoveInput = input("Please enter 'Yes' or 'No': ")
        cleaningUserRemoveInput = userRemoveInput.lstrip()
        cleaningUserRemoveInput = cleaningUserRemoveInput.rstrip()
        cleanedUserRemoveInput = cleaningUserRemoveInput.lower()
        if cleanedUserRemoveInput == 'yes' or cleanedUserRemoveInput == 'y':
            removingSelection == True
            doneRemovingGroups = False
            while doneRemovingGroups == False:
                print("\nYou can now enter a software set name from the list you would like removed")
                print("If you are done removing software sets, type 'end'")
                userRemovedSoftware = input("Please enter your selection: ")
                cleaningUserRemovedSoftware = userRemovedSoftware.lstrip()
                cleaningUserRemovedSoftware = cleaningUserRemovedSoftware.rstrip()
                cleanedUserRemovedSoftware = cleaningUserRemovedSoftware.lower()
                if cleanedUserRemovedSoftware == 'end':
                    doneRemovingGroups = True
                    removingSelection = True
                elif cleanedUserRemovedSoftware in vettedSoftwareList:
                    vettedSoftwareList.remove(cleanedUserRemovedSoftware)
                    print("You have removed %s from the list" % cleanedUserRemovedSoftware)
                else:
                    print("That softwre set was not found")
        if cleanedUserRemoveInput == 'no' or cleanedUserRemoveInput == 'n':
            removingSelection = True
        else:
            print("Input Invalid, please try again")
    print("\nFinal list of adversary software sets")
print(vettedSoftwareList)

#Save minimum number of software that share a TTP which the user wants to view. I.e. 3 == TTPs shared between 3 or more of the selected adversary software.
if cmdlineUserArgsSet == True:
    # Try-Catch block to check if the -s/--shared flag was used. If so, take cmdline input for calculations
    try:
        cmdlineShared
        if cmdlineShared <= 0 or cmdlineShared > len(vettedSoftwareList):
            print("That is not valid a number, try something between 2 and the total of selected adversary groups (inclusive)\n")
            sys.exit(2)
        else:
            userSubmittedNumber = cmdlineShared
    except NameError:
        pass
else:
    #CHECK TO MAKE SURE THE NUMBER IS NOT <=0 OR LARGER THAN THE NUMBER OF software SELECTED
    numGood = False
    while numGood == False:
        print("Enter a minimum number of software sets that need to share a TTP. For example, '3' would show all TTPs that are shared between 3 or more of the selected adversary software sets.")
        userSubmittedNumber = input("Number: ")
        try:
            userSubmittedNumber = int(userSubmittedNumber)
            if userSubmittedNumber <= 0 or userSubmittedNumber > len(vettedSoftwareList):
                print("That is not valid a number, try something between 2 and the total of selected adversary software sets (inclusive)\n")
            else:
                numGood = True
        except ValueError:
            print("This is not a valid number, please try again.\n")  

#Create array of finialized software to compare against each other
finalizedSoftware = []

#lowercase all software again in case the user used cmdline args to set specific group malware/software. This would skip the lowercase function previously, and cause the script to accidently skip software TTP matches.
vettedSoftwareList= lowerArray(vettedSoftwareList)

#Get array position of user requested software, save to "namePosition" variable
for vettedName in vettedSoftwareList:
    for namePosition in [i for i, softwareName in enumerate(editedNameArray) if softwareName == vettedName]:
        currentSoftware = Software(vettedName)
        currentSoftware.setSoftwarePosition(namePosition)
        finalizedSoftware.append(currentSoftware)

# Using the software position in the csv file, add each TTP for the software as a {'Name': 'T****} dictionary pair. This saves both the MITRE TTP designator, and the human-readable name for later comparison
for num in range(0,len(finalizedSoftware)):
    software.seek(1) # Reset to top of file to reread lines for new software
    for line in software:
        softwareArray = line.split(",")
        # The below if/else statement is used to avoid an index out of bounds exception if the user has selected the last software set in the softwareArray
        if finalizedSoftware[num].softwarePosition == len(softwareArray):
            position = finalizedSoftware[num].softwarePosition-1
        else:
            position = finalizedSoftware[num].softwarePosition
        if softwareArray[position] == '1.0' or softwareArray[position] == '1.0\n': # The '1.0\n' is needed becuase a '\n' character is added to the last entry in each line of the CSV file. This means if the user selects the last software set, any TTPs it uses will show as '1.0\n', not the usual '1.0'
            TTPDict = {softwareArray[2]:softwareArray[3]}
            finalizedSoftware[num].addSoftwareTTPs(TTPDict)

# Take all TTPs from all software and add them to a master array
totalTTPs = []
for z in range(0,len(finalizedSoftware)):
    for dictionary in finalizedSoftware[z].TTPs:
        for value in dictionary:
            totalTTPs.append(dictionary[value])

# Take master array and look for duplicates, indicating a shared TTP. Keep count for later rack n' stack
seenTTP = set()
uniqTTPWithCounts = []
for technique in totalTTPs:
    if technique not in seenTTP:
        # When a new technique is seen, create a dictionary using the technique and a counter to track number of times it overlaps between software. Then add technique to set to prvent duplicate dictionary creations 
        newDict = {technique:1}
        uniqTTPWithCounts.append(newDict)
        seenTTP.add(technique)
    else:
        for duplicateTechnique in uniqTTPWithCounts:
            for TXXXX in duplicateTechnique:
                if TXXXX == technique:
                    duplicateTechnique[TXXXX] += 1

# Select shared TTPs based on the user submitted value (N), add to an array for further processing
userRequestedTTPs = []
for sharedTTP in uniqTTPWithCounts:
    for TTPCount in sharedTTP.values():
        if TTPCount >= userSubmittedNumber :
            userRequestedTTPs.append(sharedTTP)

# Take shared TTPs from "userRequestedTTPs" and match to the TTPs used by the adversary software.
# This will create an unordered array of arrays (TTPWithCountArray) that contains all the shared TTPs between N or more software, where N is the number requested by the user
# Each array within TTPWithCountArray will be in the format [Technique Name, Technique Number, Used By X Number of software].
# For example, ['PowerShell', 'T1086', 5] would show that T1086 (aka Powershell) is shared among 5 of the user submitted adversary software, and the user has requested to see TTPs that are common among at least 5 of their selected software
# Since it runs through all the software and all the shared TTPs to make this array, there will be many duplicates. We will filter those out next.
TTPWithCountArray = []
for a in range(0,len(finalizedSoftware)):
    for dictionary in finalizedSoftware[a].TTPs:
        for key in dictionary:
            for commonTTP in userRequestedTTPs:
                for TTPnumber in commonTTP:
                    if TTPnumber == dictionary[key]:
                        for numTTPUsed in commonTTP.values():
                            nameTTPCount = [key, TTPnumber, numTTPUsed]
                            TTPWithCountArray.append(nameTTPCount)

# Here is where we filter out any duplicates, much like the first time we used set() above. However now we are just filtering, we do not need to take any other actions
seenCountedTTP = set()
uniqTTPWithCountArray = []
for b in range(0,len(TTPWithCountArray)): 
    if TTPWithCountArray[b][1] not in seenCountedTTP:
        seenCountedTTP.add(TTPWithCountArray[b][1])
        uniqTTPWithCountArray.append(TTPWithCountArray[b])

# Go through array of TTPs shared between N or more software, where N is the number requested by the user
# Start with "x" being equal to the number of software selected, then cycle through and print each TTP that is shared between "x" software
# This will start by printing TTPs shared between all software, and decrementing each cycle. For loop ends at N (number requested by the user) to prevent from wasting time going through the loop unnecessarily
# Also creates an array that has the TTPs ordered from most common to least common. This will be used when we check the TTPs to Data Sources
orderedTTPArrayByCommonality = []
for x in range(len(vettedSoftwareList),userSubmittedNumber-1,-1):
    for c in range(0,len(uniqTTPWithCountArray)):
        if uniqTTPWithCountArray[c][2] == x:
            orderedTTPArrayByCommonality.append(uniqTTPWithCountArray[c])

# This section prints information useful to the user, before continuing on and comparing the results to applicable data sources
print("Total selected software: %d" % len(finalizedSoftware))
print("User requested to see TTPs shared between %d or more adversary software" % userSubmittedNumber)
print("Total unique TTPs used between all software: %d" % len(uniqTTPWithCounts))
print("Total TTPs that are shared between %d or more adversary software: %d" % (userSubmittedNumber,len(orderedTTPArrayByCommonality)))
TTPObjectArray = []
for d in range(len(orderedTTPArrayByCommonality)):
    print("%s, aka %s is shared among %d/%d adversary software" % (orderedTTPArrayByCommonality[d][0],orderedTTPArrayByCommonality[d][1],orderedTTPArrayByCommonality[d][2],len(finalizedSoftware)))
    # Create TTP objects, which will be used below
    currentTTP = TTP(orderedTTPArrayByCommonality[d][0],orderedTTPArrayByCommonality[d][1],orderedTTPArrayByCommonality[d][2])
    TTPObjectArray.append(currentTTP)

for t in range(0,len(TTPObjectArray)):
        dataSources.seek(1)
        for line in dataSources:
            dataSourcesArray = line.split(",")
            if TTPObjectArray[t].number == dataSourcesArray[3]:
                for u in range(0,len(dataSourcesArray)):
                    if dataSourcesArray[u] == '1' or dataSourcesArray[u] == '1\n': # The '1\n' is needed becuase a '\n' character is added to the last entry in each line of the CSV file. This means if a TTP can be found using the last data source in the list, it will show as '1\n', not the usual '1'
                        TTPObjectArray[t].addDataSource(u)

# Begin operations with data sources
exitDataSourceOperations = False
while exitDataSourceOperations == False:
    print()
    exitDataSourceOperations = dataSourceOperations(TTPObjectArray,dataSourceNameArray,len(finalizedSoftware),cmdlineUserArgsSet)
print("Closing")