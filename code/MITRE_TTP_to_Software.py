# Author: eTAFTo
# Date: August 15, 2019
# Updated: July 17, 2020 by aptgetKubert
# Title: Extraction of MITRE ATT&CK Matrix (TTP to Software)
# pip install attackcti

from attackcti import attack_client
from pandas.io.json import json_normalize
import pandas as pd
import numpy as np

lift = attack_client()
enterprise_software = lift.get_software(stix_format=False)

# Created empty dataframe
t2s_df = pd.DataFrame()

for i in range(0, len(enterprise_software)):
    software_name = enterprise_software[i]['software']
        
    tech_by_software = lift.get_techniques_used_by_software(enterprise_software[i], stix_format=False)
    t2s_normalized = json_normalize(tech_by_software)

    # created a filter for platform (a.k.a OS)
    if "platform" in t2s_normalized.columns:

        # filter for platforms with Windows Only
        platform_list = []
        t2s_normalized['platform'] = t2s_normalized['platform'].replace(np.nan, 0)
        for sublst in t2s_normalized['platform']:
            if sublst == 0:
                platform_list.append(0)
            else:
                platform_list.append(", ".join(sublst))
        t2s_normalized['platform'] = platform_list
        t2s_normalized = t2s_normalized[t2s_normalized['platform'].str.contains("Windows").notnull()]

        # Create the data frame (add new column with null values; created the dataframe, added binary values to the group, and dropped null values)
        t2s_normalized[software_name] = np.nan
        t2s_normalized = pd.DataFrame(t2s_normalized[['tactic', 'technique', 'technique_id', software_name]])
        t2s_normalized[software_name] = 1
        t2s_normalized = t2s_normalized.dropna()

        # extracted string from tactics: converted to strings, then created a column with string values
        tactic_list = []
        for sublst in t2s_normalized['tactic']:
            tactic_list.append("| ".join(sublst))
        t2s_normalized['tactic'] = tactic_list

        # Merge tables
        if i == 0: 
            # populated the main table
            t2s_df = t2s_normalized
        else:  
            # Merged tables on three columns
            t2s_df = t2s_df.merge(t2s_normalized, how='outer', on=['tactic', 'technique', 'technique_id'])
            print('Progress : ' + str(i) + ' of ' + str(len(enterprise_software)))

# Renamed the Index
t2s_df = t2s_df.shift()[1:]

# Print the shape
t2s_df.shape

# Create CSV
t2s_df.to_csv("MITRE_software_output.csv")