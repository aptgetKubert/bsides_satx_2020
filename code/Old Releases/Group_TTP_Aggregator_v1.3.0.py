# Author: aptgetKubert
# Date: June 16, 2020
# Purpose: Offline aggregation of adversary groups and mapping to data sources that can be monitored and potentially ingested into a SIEM  
# pip install attackcti

import collections

groups = open("MITRE_group_output.csv","r") 
dataSources = open("MITRE_data_sources_output.csv","r")

# Create class "Group" that will store the TTPs that each groups uses for comparison later
class Group:
    'Common class for all threat actor groups'
    # As class variables, if these are directly changed, they wil be changed for ALL objects. See below for methods to change variables for each object
    groupCount = 0 # This will be used to track number of groups that a user has requested to be compared
    groupPosition = 0 # Used to save position of group in "group_output.csv". This allows us to tie specific TTPs to a particular group

    def __init__(self, name):
        self.name = name
        self.TTPs = [] # Array for known TTPs to be stored for later comparison
        Group.groupCount += 1 # Update total number of group objects each time one is created

    def showCount(self):
        print("Total groups requested by user: %d" % Group.groupCount)
    
    def showTTPs(self):
        print(self.TTPs)

    def setGroupPosition(self, pos):
        self.groupPosition = pos
    
    def addGroupTTPs(self, TTPPair):
        self.TTPs.append(TTPPair)

# Createclass "TTP" that will store the ID number, name, and count in terms of use based on the user slected adversary groups
# Also will store data sources that can be used to discover this particular TTP.
class TTP:
    'Common class for all TTPs'

    def __init__(self, name, number, count):
        self.name = name # I.e. Account Access Removal
        self.number = number # I.e. T1531
        self.TTPCount = count # Tracks number of groups selected by the user which use this TTP
        self.dataSources = [] # Array to hold positions, which can be later matached to specific Data Sources
    
    def addDataSource(self, dataSourcePosition):
        self.dataSources.append(dataSourcePosition)

# Switch case statements allowing the user to conduct various operations with the common TTPs and data sources that can be used to find them
# Options are: List # of TTPs each data source can find, show which data sources can be used to find each TTP, and exit
def dataSourceOperations(TTPObjectList,dataSourceNames,numOfSelectedGroups):
    print("Enter '1' if you want to see the total number of common TTPs each data source can be used to find")
    print("Enter '2' if you want to see each common TTP and the data source(s) that can be used to find them")
    print("Enter '3' to exit")
    userOperationIsGood = False
    while userOperationIsGood == False:
        userSubmittedOperation = input("Please enter your choice: ")
        print()
        try:
            userSubmittedOperation = int(userSubmittedOperation)
            if userSubmittedOperation >= 1 and userSubmittedOperation <= 3:
                userOperationIsGood = True
            else:
                print("That is not valid a number, try something between 1 and 3 (inclusive)\n")
        except ValueError:
            print("This is not a valid number, please try again.\n")
    
    # List # of shared TTPs each data source can be used to find
    # First create array with the data sorce positions, this will make it easier to convert them to to data source names
    if userSubmittedOperation == 1:
        totalDataSources = []
        for v in range(0,len(TTPObjectList)):
            for w in range(0,len(TTPObjectList[v].dataSources)):
                totalDataSources.append(TTPObjectList[v].dataSources[w])

        # Match positions to actual data source names
        for aa in range(0,len(totalDataSources)):
            for bb in range(4,len(dataSourceNames)):
                if totalDataSources[aa] == bb:
                    totalDataSources[aa] = dataSourceNames[bb]
        
        # Creates a dictionary that tracks the number of times a data source is seen among all of the shared TTPs
        dataSourcesCountDict = {cc:totalDataSources.count(cc) for cc in totalDataSources}

        # Loop through the TTP objects and print in decending order the data sources and number of shared TTPs they can potentially find
        for dd in range(len(TTPObjectList),0,-1):
            for dataSource, dataSourceCount in dataSourcesCountDict.items():
                if dataSourceCount == dd and dataSource != ',':
                    print("%s can be used to potentially find %s/%d shared TTPs\n" % (dataSource,dataSourceCount,len(TTPObjectList)))
        return(False)
    
    # Print each shared TTP and the data sources that can be used to find it. MAY RESULT IN A LOT OF TEXT
    elif userSubmittedOperation == 2:
        # Loop through all TTP objects, and loop through all their data sources to convert position numbers to data source names
        for ee in range(0,len(TTPObjectList)):
            for ff in range(0,len(TTPObjectList[ee].dataSources)):
                for gg in range(4,len(dataSourceNames)):
                    if TTPObjectList[ee].dataSources[ff] == gg:
                        TTPObjectList[ee].dataSources[ff] = dataSourceNames[gg]
        
        # Print each TTP name followed by their associated data sources
        for hh in range(0,len(TTPObjectList)):
            print("%s can be potentially found using the following data source(s)" % TTPObjectList[hh].name)
            for ii in range(0,len(TTPObjectList[hh].dataSources)):
                print("\t%s" % TTPObjectList[hh].dataSources[ii])
        return(False)

    # Exit data source operations, and exits program
    elif userSubmittedOperation == 3:
        return(True)

# This function allows for a user to add a group of adversary groups based on country or target industry
def bulkGroupAdd(countryArray,industryArray,existingGroupsArray):
    doneAddingByCountry = False
    addedCountryCode = []
    while doneAddingByCountry == False:
        print("You may now select adversary groups in bulk based off suspected country of origin or target industry.")
        print("Select 1 for %s, 2 for %s, 3 for %s, 4 for %s, 5 for %s, and 6 to skip to target industry." % (countryArray[0][0],countryArray[1][0],countryArray[2][0],countryArray[3][0],countryArray[4][0]))
        userSubmittedCountry = input("Number: ")
        # Make sure the user input is a number
        try:
            userSubmittedCountry = int(userSubmittedCountry)
            # Make sure the user input is a valid number and hasn't been selected already to eliminate possible duplicates
            if userSubmittedCountry <= 0 or userSubmittedCountry >= 7:
                print("That is not valid a number, try something between 1 and 6 (inclusive)\n")
            elif userSubmittedCountry in addedCountryCode:
                print("You have already added that country!\n")
            elif userSubmittedCountry == 6:
                doneAddingByCountry = True
            else:
                addedCountryCode.append(userSubmittedCountry)
                for jj in range(1,len(countryArray[userSubmittedCountry-1])):
                    existingGroupsArray.append(countryArray[userSubmittedCountry-1][jj])
                print("You have added groups suspected to be from: %s\n" % countryArray[userSubmittedCountry-1][0])
        except ValueError:
            print("This is not a valid number, please try again.\n")
    doneAddingByIndustry = False
    addedIndustryCode = []
    while doneAddingByIndustry == False:
        print("You may now select adversary groups in bulk based off target industry")
        print("Select 1 for %s, 2 for %s, 3 for %s, 4 for %s, 5 for %s," % (industryArray[0][0],industryArray[1][0],industryArray[2][0],industryArray[3][0],industryArray[4][0]))
        print("Select 6 for %s, 7 for %s, 8 for %s, 9 for %s, 10 for %s," % (industryArray[5][0],industryArray[6][0],industryArray[7][0],industryArray[8][0],industryArray[9][0]))
        print("Select 11 for %s, 12 for %s, 13 for %s, 14 for %s, 15 for %s," % (industryArray[10][0],industryArray[11][0],industryArray[12][0],industryArray[13][0],industryArray[14][0]))
        print("Select 16 for %s, 17 for %s, 18 for %s, 19 for %s, 20 for %s" % (industryArray[15][0],industryArray[16][0],industryArray[17][0],industryArray[18][0],industryArray[19][0]))
        print("If you would like to skip this step, enter 0 ")
        userSubmittedIndustry = input("Number: ")
        try:
            userSubmittedIndustry = int(userSubmittedIndustry)
            # Make sure the user input is a valid number and hasn't been selected already to eliminate possible duplicates
            if userSubmittedIndustry < 0 or userSubmittedIndustry > 20:
                print("That is not valid a number, try something between 0 and 20 (inclusive)\n")
            elif userSubmittedIndustry in addedIndustryCode:
                print("You have already added that industry!\n")
            elif userSubmittedIndustry == 0:
                doneAddingByIndustry = True
            else:
                addedIndustryCode.append(userSubmittedIndustry)
                for ll in range(1,len(industryArray[userSubmittedIndustry-1])):
                    existingGroupsArray.append(industryArray[userSubmittedIndustry-1][ll])
                if userSubmittedIndustry == 20:
                    print("You have added groups that have targeted non-US countries\n")
                else:
                    print("You have added groups that have targeted the %s industry\n" % industryArray[userSubmittedIndustry-1][0])
        except ValueError:
            print("This is not a valid number, please try again.\n")
    return existingGroupsArray

#Save first line of Group Output CSV with all group names, save to a list
firstGroupRow = next(groups)
nameArray = firstGroupRow.split(",")

# Normalize values for later checking against user input
editedNameArray = []
for n in range(len(nameArray)):
    editedNameArray.append(nameArray[n].lower())
    # The CSV file adds a '\n' character to the last string in nameArray. This will cause it to not match should someone want to compare that group. 
    # This if statement checks if we are at the end of the array, and if so to remove the last character in the string, which will be '\n'.
    if n == len(nameArray)-1:
        editedNameArray[n] = editedNameArray[n][:-1]

# Save first line of Data Sources Output CSV, with all the MITRE ID'd data sources, save to a list
firstDataSourcesRow = next(dataSources)
dataSourceNameArray = firstDataSourcesRow.split(",")
# Take the last entry in the "dataSourceNameArray" and remove the newline character
dataSourceNameArray[len(dataSourceNameArray)-1] = dataSourceNameArray[len(dataSourceNameArray)-1][:-1]

# Array of arrays for "Associated Groups", formerly known as "Aliases". This helps check user input against other associated group names.
# Currently I make this manually. I'm sure there is a way to automate this, just havent done it yet.
associatedGroupArray = [['admin@338','temper panda','admin338','team338','338 team'],['apt-c-36','blind eagle'],['apt1','comment crew','comment group','comment panda'],['apt12','ixeshe','dyncalc','numbered panda','dnscalc'],['apt17','deputy dog'],['apt18','tg-0416','dynamite panda','threat group-0416'],['apt19','codoso','c0d0so0','codoso team','sunshop group'],['apt28','snakemackerel','swallowtail','group 74','sednit','sofacy','pawn storm','fancy bear','strontium','tsar team','threat group-4127','tg-4127'],['apt29','yttrium','the dukes','cozy bear','cozyduke'],['apt3','gothic panda','pirpi','ups team','buckeye','threat group-0110','tg-0110'],['apt32','sealotus','oceanlotus','apt-c-00'],['apt33','elfin'],['apt37','scarcruft','reaper','group123','temp.reaper'],['apt39','chafer'],['axiom','group72'],['bronze butler','redbaldknight','tick'],['carbanak','anunak','carbon spider'],['cleaver','threat group 2889','tg-2889'],['cobalt group','cobalt gang','cobalt spider'],['deep panda','shell crew','webmasters','kungfu kittens','pinkpanther','black vine'],['dragonfly','energetic bear'],['dragonfly 2.0','berserk bear'],['elderwood','elderwood gang','beijing group','sneaky panda'],['fin6','itg08'],['inception','inception framework','cloud atlas'],['ke3chang','apt15','mirage','vixen panda','gref','playful dragon','royalapt'],['kimsuky','velvet chollima'],['lazarus group','hidden cobra','guardians of peace','zinc','nickel academy'],['leafminer','raspite'],['leviathan','temp.jumper','apt40','temp.periscope'],['lotus blossom','dragonfish','spring dragon'],['machete','el machete'],['magic hound','rocket kitten','operation saffron rose','ajax security team','operation woolen-goldfish','newscaster','cobalt gypsy','apt35'],['menupass','stone panda','apt10','red apollo','cvnx','hogfish'],['molerats','operation molerats','gaza cybergang'],['muddywater','seedworm','temp.zagros'],['oilrig','irn2','helix kitten','apt34'],['patchwork','dropping elephant','chinastrats','monsoon','operation hangover'],['putter panda','apt2','msupdater'],['sandworm team','quedagh','voodoo bear'],['strider','projectsauron'],['temp.veles','xenotime'],['threat group-1314','tg-1314'],['threat group-3390','tg-3390','emissary panda','bronze union','apt27','iron tiger','luckymouse'],['tropic trooper','keyboy'],['turla','waterbug','whitebear','venomous bear','snake','krypton'],['windshift','bahamut'],['winnti group','blackfly'],['wizard spider','temp.mixmaster','grim spider']]

# Save group name the user wants to view
vettedGroupList = []

# Begin bulk add of groups
# These arrays are currently made manually. To automate it would require a script to look for certain keywords in the MITRE group descriptions. Probably possible, just haven't done it yet
groupByCountryArray = [['China','admin@338','apt1','apt12','apt16','apt17','apt18','apt19','apt3','apt30','apt41','axiom','bronzebutler','deep panda','elderwood','ke3chang','menupass','moafee','mofang','naikon','night dragon','pittytiger','putter panda','rocke','soft cell','suckfly','ta459','threat group-3390','winnti group'],['Russia','apt28','apt29','dragonfly 2.0','sandworm team','temp.veles','turla'],['DPRK','apt37','apt38','kimsuky','lazarus group','stolen pencil'],['Iran','apt33','apt39','charming kitten','cleaver','copykittens','group 5','leafminer','magic hound','muddywater','oilrig'],['Other','apt-c-36','apt32','blackoasis','dark caracal','silverterrier']]
groupsByIndustryArray = [['Finance','admin@338','apt-c-36','apt19','apt38','carbanak','cobalt group','darkvishnya','deep panda','fin4','gcman','oilrig','rtm','silence','sharpshooter'],['High Tech','apt12','apt17','apt18','apt19','apt41','elderwood','magic hound','menupass','silverterrier','threat group-3390','tropic trooper'],['Media','apt12','blackoasis','charming kitten','sandworm team'],['Government','apt12','apt17','apt18','apt28','apt29','apt-c-36','darkhydrus','deep panda','dragonfly 2.0','gallmaker','gorgon group','inception','ke3chang','leafminer','leviathan','lotus blossom','magic hound','menupass','mofang','molerats','muddywater','oilrig','patchwork','sandworm team','sowbug','threat group-3390','tropic trooper','turla','windshift'],['Defense','apt17','apt19','deep panda','dragonfly','elderwood','gallmaker','leviathan','menupass','sharpshooter','threat group-3390','thrip','turla'],['NGOs','apt17','apt18','charming kitten','elderwood','honeybee','scarlet mimic'],['Law Firms','apt17','apt19'],['Medical/Healthcare','apt18','apt41','deep panda','fin4','menupass','orangeworm','tropic trooper','whitefly'],['Energy','apt19','apt33','dragonfly','magic hound','oilrig','sandworm team','sharpshooter','threat group-3390'],['Pharmaceuticals','apt19','fin4','turla'],['Telecommunications','apt19','apt39','apt41','deep panda','muddywater','oilrig','soft cell','thrip'],['Education','apt19','charming kitten','darkhydrus','leviathan','menupass','silverterrier','stolen pencil','turla'],['Manufacturing','apt18','apt19','apt-c-36','leviathan','menupass','silverterrier','threat group-3390'],['Aviation','apt33','dragonfly','menupass','threat group-3390'],['Hospitality','darkhotel','fin5','fin6','fin7','fin8'],['Critical Infrastructure','dragonfly','dragonfly 2.0','mofang','sandworm team','temp.veles','windshift'],['Oil','apt-c-36','ke3chang','muddywater'],['Video Games','apt41','fin5','winnti group'],['Diplomatic','patchwork','write','turla'],['International','apt16','apt3','apt32','apt33','apt37','apt38','apt41','apt-c-36','blacktech','bronzebutler','charming kitten','cobalt group','copykittens','darkhydrus','darkvishnya','dragonok','dust storm','gallmaker','gamaredon group','gorgon group','group 5','honeybee','inception','kimsuky','leviathan','lotus blossom','machete','magic hound','menupass','mofang','muddywater','naikon','neodymium','oilrig','orangeworm','platinum','promethium','rancor','rtm','sandworm team','silence','sowbug','stealth falcon','strider','ta459','taidoor','the white company','thrip','tropic trooper','turla','windshift','whitefly','write']]
print()
vettedGroupList = bulkGroupAdd(groupByCountryArray,groupsByIndustryArray,vettedGroupList)

# Test case groups: apt41, axiom, cozy bear, soft cell, fin8, blargblarg
userContinue = False
print("Please enter the name of the APT groups you would like to compare. For groups such as APT## do not add a space between 'APT' and '##'. When you are done type 'end'.")
while userContinue == False:
    userSubmittedGroupName = input("Enter APT group name: ")
    cleaningUserSubmittedGroupName = userSubmittedGroupName.lstrip() #Remove leading spaces
    cleaningUserSubmittedGroupName = cleaningUserSubmittedGroupName.rstrip() #Remove trailing spaces
    cleaningUserSubmittedGroupName = cleaningUserSubmittedGroupName.lower() #Convert string to lowercase

    userInputIsSanitized = False
    
    #Check if the user is done entering names
    if cleaningUserSubmittedGroupName == 'end':
        userContinue = True
        userInputIsSanitized = True
    
    # Check if user input is already in the group name array
    for q in range(4,len(editedNameArray)):
        if cleaningUserSubmittedGroupName == editedNameArray[q]:
            userInputIsSanitized = True
            break
    
    # Check if a user entered a name of an associated group, if so change it to the MITRE group name.
    if userInputIsSanitized == False:
        possibleAssociatedGroup = cleaningUserSubmittedGroupName
        for r in range(0,len(associatedGroupArray)):
            for s in range(1,len(associatedGroupArray[r])):
                if cleaningUserSubmittedGroupName == associatedGroupArray[r][s]:
                    cleaningUserSubmittedGroupName = associatedGroupArray[r][0]
                    userInputIsSanitized = True
    
        # If the user entered an associated group name, tell the user that the name has been switched
        if possibleAssociatedGroup != cleaningUserSubmittedGroupName:
            print("You originally entered %s, which is an associated group of %s." % (possibleAssociatedGroup,cleaningUserSubmittedGroupName))
            print("Your entry has been changed to %s in order to compare TTPs" % cleaningUserSubmittedGroupName)
    
    # If userInputIsSanitized is still false at this point, it means that the group name was not found in the MITRE list, nor one of the associated groups.
    if userInputIsSanitized == False:
        print("Your input of '%s' does not seem to match any group or associated group names. Please try again" % cleaningUserSubmittedGroupName)
    else:
        #Save input to array of "scrubbed" group names, increment number of valid entries
        if cleaningUserSubmittedGroupName != 'end':
            vettedGroupList.append(cleaningUserSubmittedGroupName)

# Remove any duplicate groups
duplicatesRemoved = set()
uniqGroups = []
for mm in vettedGroupList:
    if mm not in duplicatesRemoved:
        uniqGroups.append(mm)
        duplicatesRemoved.add(mm)
vettedGroupList = uniqGroups

# Allow user to remove any groups, then print vettedGroupList and number of valid groups
print("\nHere is your final list of %d valid adversary groups" % len(vettedGroupList))
print(vettedGroupList)
print("Would you like to remove any groups from the list?")
userRemoveInput = input("Please enter 'Yes' or 'No': ")
cleaningUserRemoveInput = userRemoveInput.lstrip()
cleaningUserRemoveInput = cleaningUserRemoveInput.rstrip()
cleanedUserRemoveInput = cleaningUserRemoveInput.lower()
removingSelection = False
while removingSelection == False:
    if cleanedUserRemoveInput == 'yes' or cleanedUserRemoveInput == 'y':
        removingSelection == True
        doneRemovingGroups = False
        while doneRemovingGroups == False:
            print("\nYou can now enter a group name from the list you would like removed")
            print("If you are done removing groups, type 'end'")
            userRemovedGroup = input("Please enter your selection: ")
            cleaningUserRemovedGroup = userRemovedGroup.lstrip()
            cleaningUserRemovedGroup = cleaningUserRemovedGroup.rstrip()
            cleanedUserRemovedGroup = cleaningUserRemovedGroup.lower()
            if cleanedUserRemovedGroup == 'end':
                doneRemovingGroups = True
                removingSelection = True
            elif cleanedUserRemovedGroup in vettedGroupList:
                vettedGroupList.remove(cleanedUserRemovedGroup)
                print("You have removed %s from the list" % cleanedUserRemovedGroup)
            else:
                print("That group name was not found")
    elif cleanedUserRemoveInput == 'no' or cleanedUserRemoveInput == 'n':
        removingSelection = True
    else:
        print("Input Invalid, please try again")
print("\nFinal list of adversary groups")
print(vettedGroupList)

#Save minimum number of groups that share a TTP which the user wants to view. I.e. 3 == TTPs shared between 3 or more of the selected adversary groups.
#CHECK TO MAKE SURE THE NUMBER IS NOT <=0 OR LARGER THAN THE NUMBER OF GROUPS SELECTED
numGood = False
while numGood == False:
    print("\nEnter a minimum number of groups that need to share a TTP. For example, '3' would show all TTPs that are shared between 3 or more of the selected adversary groups.")
    userSubmittedNumber = input("Number: ")
    try:
        userSubmittedNumber = int(userSubmittedNumber)
        if userSubmittedNumber <= 0 or userSubmittedNumber > len(vettedGroupList):
            print("That is not valid a number, try something between 2 and the total of selected adversary groups (inclusive)\n")
        else:
            numGood = True
    except ValueError:
        print("This is not a valid number, please try again.\n")  

#Create array of finialized groups to compare against each other
finalizedGroups = []

#Get array position of user requested groups, save to "namePosition" variable
for vettedName in vettedGroupList:
    for namePosition in [i for i, groupName in enumerate(editedNameArray) if groupName == vettedName]:
        currentGroup = Group(vettedName)
        currentGroup.setGroupPosition(namePosition)
        finalizedGroups.append(currentGroup)

# Using the group position in the csv file, add each TTP for the group as a {'Name': 'T****} dictionary pair. This saves both the MITRE TTP designator, and the human-readable name for later comparison
for num in range(0,len(finalizedGroups)):
    groups.seek(1) # Reset to top of file to reread lines for new group
    for line in groups:
        groupArray = line.split(",")
        # The below if/else statement is used to avoid an index out of bounds exception if the user has selected the last group in the groupArray
        if finalizedGroups[num].groupPosition == len(groupArray):
            position = finalizedGroups[num].groupPosition-1
        else:
            position = finalizedGroups[num].groupPosition
        if groupArray[position] == '1.0' or groupArray[position] == '1.0\n': # The '1.0\n' is needed becuase a '\n' character is added to the last entry in each line of the CSV file. This means if the user selects the last group, any TTPs it uses will show as '1.0\n', not the usual '1.0'
            TTPDict = {groupArray[2]:groupArray[3]}
            finalizedGroups[num].addGroupTTPs(TTPDict)

# Take all TTPs from all groups and add them to a master array
totalTTPs = []
for z in range(0,len(finalizedGroups)):
    for dictionary in finalizedGroups[z].TTPs:
        for value in dictionary:
            totalTTPs.append(dictionary[value])

# Take master array and look for duplicates, indicating a shared TTP. Keep count for later rack n' stack
seenTTP = set()
uniqTTPWithCounts = []
for technique in totalTTPs:
    if technique not in seenTTP:
        # When a new technique is seen, create a dictionary using the technique and a counter to track number of times it overlaps between groups. Then add technique to set to prvent duplicate dictionary creations 
        newDict = {technique:1}
        uniqTTPWithCounts.append(newDict)
        seenTTP.add(technique)
    else:
        for duplicateTechnique in uniqTTPWithCounts:
            for TXXXX in duplicateTechnique:
                if TXXXX == technique:
                    duplicateTechnique[TXXXX] += 1

# Select shared TTPs based on the user submitted value (N), add to an array for further processing
userRequestedTTPs = []
for sharedTTP in uniqTTPWithCounts:
    for TTPCount in sharedTTP.values():
        if TTPCount >= userSubmittedNumber :
            userRequestedTTPs.append(sharedTTP)

# Take shared TTPs from "userRequestedTTPs" and match to the TTPs used by the adversary groups.
# This will create an unordered array of arrays (TTPWithCountArray) that contains all the shared TTPs between N or more groups, where N is the number requested by the user
# Each array within TTPWithCountArray will be in the format [Technique Name, Technique Number, Used By X Number of Groups].
# For example, ['PowerShell', 'T1086', 5] would show that T1086 (aka Powershell) is shared among 5 of the user submitted adversary groups, and the user has requested to see TTPs that are common among at least 5 of their selected groups
# Since it runs through all the groups and all the shared TTPs to make this array, there will be many duplicates. We will filter those out next.
TTPWithCountArray = []
for a in range(0,len(finalizedGroups)):
    for dictionary in finalizedGroups[a].TTPs:
        for key in dictionary:
            for commonTTP in userRequestedTTPs:
                for TTPnumber in commonTTP:
                    if TTPnumber == dictionary[key]:
                        for numTTPUsed in commonTTP.values():
                            nameTTPCount = [key, TTPnumber, numTTPUsed]
                            TTPWithCountArray.append(nameTTPCount)

# Here is where we filter out any duplicates, much like the first time we used set() above. However now we are just filtering, we do not need to take any other actions
seenCountedTTP = set()
uniqTTPWithCountArray = []
for b in range(0,len(TTPWithCountArray)): 
    if TTPWithCountArray[b][1] not in seenCountedTTP:
        seenCountedTTP.add(TTPWithCountArray[b][1])
        uniqTTPWithCountArray.append(TTPWithCountArray[b])

# Go through array of TTPs shared between N or more groups, where N is the number requested by the user
# Start with "x" being equal to the number of groups selected, then cycle through and print each TTP that is shared between "x" groups
# This will start by printing TTPs shared between all groups, and decrementing each cycle. For loop ends at N (number requested by the user) to prevent from wasting time going through the loop unnecessarily
# Also creates an array that has the TTPs ordered from most common to least common. This will be used when we check the TTPs to Data Sources
orderedTTPArrayByCommonality = []
for x in range(len(vettedGroupList),userSubmittedNumber-1,-1):
    for c in range(0,len(uniqTTPWithCountArray)):
        if uniqTTPWithCountArray[c][2] == x:
            orderedTTPArrayByCommonality.append(uniqTTPWithCountArray[c])

# This section prints information useful to the user, before continuing on and comparing the results to applicable data sources
print("Total selected groups: %d" % len(finalizedGroups))
print("NOTE: If the number of the selected groups != the list of valid adversaries,")
print("\tit most likely means some groups only have PRE-ATT&CK TTPs, which are not currently captured with this script")
print("User requested to see TTPs shared between %d or more adversary groups" % userSubmittedNumber)
print("Total unique TTPs used between all groups: %d" % len(uniqTTPWithCounts))
print("Total TTPs that are shared between %d or more adversary groups: %d" % (userSubmittedNumber,len(orderedTTPArrayByCommonality)))
TTPObjectArray = []
for d in range(len(orderedTTPArrayByCommonality)):
    print("%s (%s) is shared among %d/%d adversary groups" % (orderedTTPArrayByCommonality[d][0],orderedTTPArrayByCommonality[d][1],orderedTTPArrayByCommonality[d][2],len(finalizedGroups)))
    # Create TTP objects, which will be used below
    currentTTP = TTP(orderedTTPArrayByCommonality[d][0],orderedTTPArrayByCommonality[d][1],orderedTTPArrayByCommonality[d][2])
    TTPObjectArray.append(currentTTP)

for t in range(0,len(TTPObjectArray)):
        dataSources.seek(1)
        for line in dataSources:
            dataSourcesArray = line.split(",")
            if TTPObjectArray[t].number == dataSourcesArray[3]:
                for u in range(0,len(dataSourcesArray)):
                    if dataSourcesArray[u] == '1' or dataSourcesArray[u] == '1\n': # The '1\n' is needed becuase a '\n' character is added to the last entry in each line of the CSV file. This means if a TTP can be found using the last data source in the list, it will show as '1\n', not the usual '1'
                        TTPObjectArray[t].addDataSource(u)

# Begin operations with data sources
exitDataSourceOperations = False
while exitDataSourceOperations == False:
    print()
    exitDataSourceOperations = dataSourceOperations(TTPObjectArray,dataSourceNameArray,len(finalizedGroups))
print("Closing")