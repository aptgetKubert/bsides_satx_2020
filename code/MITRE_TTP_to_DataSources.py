# Author: eTAFTo
# Date: August 19, 2019
# Title: Extraction of MITRE ATT&CK Matrix (TTP to Source)
# pip install attackcti

from attackcti import attack_client
from pandas.io.json import json_normalize
import pandas as pd
import numpy as np

lift = attack_client()

enterprise_techniques = lift.get_enterprise_techniques(stix_format=False)
techniques_normalized = json_normalize(enterprise_techniques)

t2ds_df = techniques_normalized[['tactic', 'technique', 'technique_id', 'data_sources']]

# extracted string from tactics: converted to strings, then created a column with string values
tactic_list = []
for sublst in t2ds_df['tactic']:
    if isinstance(sublst, list):
        tactic_list.append("| ".join(sublst))
    else:
        tactic_list.append("No Tactic Available")
t2ds_df['tactic'] = tactic_list

#Create Empty Data Frame
ds_subset_df = pd.DataFrame()

# Create DataFrame
for i in range( 0, len(t2ds_df['data_sources'])):
    if type(t2ds_df['data_sources'][i]) != float:
        for source in t2ds_df['data_sources'][i]:       
            if type(source) == str:
                ds_subset_df['technique_id'] = t2ds_df['technique_id']
                ds_subset_df[source] = "" 

# Populate Dataframe
for i in range( 0, len(t2ds_df['data_sources'])):
    if type(t2ds_df['data_sources'][i]) != float:
        for source in t2ds_df['data_sources'][i]:       
            if type(source) == str:
                ds_subset_df.loc[i,source] = str(1)

# Drop data source column
t2ds_df= t2ds_df.drop(['data_sources'], axis=1)

# Merge DataFrames
t2ds_df = t2ds_df.merge(ds_subset_df, how='outer', on=['technique_id'])

# Renamed the Index
t2ds_df = t2ds_df.shift()[1:]

# Print the shape
print(t2ds_df.shape)

# Create CSV
t2ds_df.to_csv("MITRE_data_sources_output.csv")