# Author: eTAFTo
# Date: August 22, 2019
# Updated: July 18, 2020
# Title: Extraction of MITRE ATT&CK Matrix (Groups to Software)
# pip install attackcti

from attackcti import attack_client
from pandas.io.json import json_normalize
import pandas as pd
import numpy as np

lift = attack_client()
enterprise_groups = lift.get_enterprise_groups(stix_format=False)

# Create emtpy dictionary
new_dictionary = {}

y = 1
# iterate thru all enterprise groups
for i in range(0, len(enterprise_groups)):

    # extract actual group name
    group_name = enterprise_groups[i]['group']
    
    # extract software by group using API
    software_by_group = lift.get_software_used_by_group(enterprise_groups[i], stix_format=False)
    
    # normalize json file
    g2s_normalized = json_normalize(software_by_group)

    # create dictionary by software list
    new_dictionary[group_name] = g2s_normalized['software'].to_list()
    print("Finished %d software set(s), Part 1" % y)
    y += 1

# create variables with keys or values
columns = list(new_dictionary.keys())
values = list(new_dictionary.values())
arr_len = len(values)

# create dataframe with group as column names and list of software as rows
g2s_df = pd.DataFrame(np.array(values, dtype=object).reshape(1, arr_len), columns=columns)

# create empty dataframe
g2s_df_final = pd.DataFrame()
df = pd.DataFrame()

# create empty dictionary
soft_dict = {}

z = 1
# iterate by column 
for column in g2s_df:
    # There are gruops that do not have software, however the API defaults to these groups using ALL software
    # Filtered the grups if list of software is greater than 250
    if len(list(g2s_df[column][0])) > 250:
        pass
    else:
        # create variable with list of software per column
        soft_list = g2s_df[column][0]
        
        # create variable with values, binary, given length of list
        group_val = [1] * len(g2s_df[column][0])
        
        # create dictionary using soft_list and group_val variables
        soft_dict = {'software': soft_list, column: group_val}
        
        # create dataframe using dictionary
        df = pd.DataFrame(soft_dict)
        
        # Conditional statement to generate new dataframe with first pass
        if len(g2s_df_final.columns) == 0:
            g2s_df_final = df
        else:
            # merge data frames together, on software
            g2s_df_final = g2s_df_final.merge(df, how='outer', on=['software'])
            print("Finished %d software set(s), Part 2" % z)
            z += 1
# Renamed the Index
g2s_df_final = g2s_df_final.shift()[1:]

# Print the shape
print(g2s_df_final.shape)

# Create CSV
g2s_df_final.to_csv("MITRE_software_to_groups_output.csv")